using System.Collections;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]
public class particleAttractorMove : MonoBehaviour {
	ParticleSystem ps;
	ParticleSystem.Particle[] m_Particles;
	public Transform target;
	public float speed = 5f;
	int numParticlesAlive;
	void Start () {
		ps = GetComponent<ParticleSystem>();
		if (!GetComponent<Transform>()){
			GetComponent<Transform>();
		}
	}
	void Update () {
		m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
		numParticlesAlive = ps.GetParticles(m_Particles);
		float step = speed * Time.deltaTime;
		for (int i = 0; i < numParticlesAlive; i++) {
			m_Particles[i].position = Vector3.MoveTowards(m_Particles[i].position, target.position, step);
			float particleDistToTarget = Vector3.Distance(m_Particles[i].position, target.position);
			//fade color depending on distance to target
			m_Particles[i].startColor = new Color(m_Particles[i].startColor.r / 255.0f, 
												  m_Particles[i].startColor.g / 255.0f, 
												  m_Particles[i].startColor.b / 255.0f, 
												  Mathf.Min(particleDistToTarget / 4.0f, m_Particles[i].startColor.a / 255.0f));

			if (Vector3.Distance(m_Particles[i].position, target.position) < 0.01f)
				m_Particles[i].remainingLifetime = 0.0f;
		}
		ps.SetParticles(m_Particles, numParticlesAlive);
	}
}
