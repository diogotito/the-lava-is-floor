﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class MotherSlimeMoviment : Controllable
{
    public GameObject BabySlimePrefab;
    public int InitialNumberOfLifes = 5;
    
    public int CurrentNumberOfLifes;
    public GameObject CurrentSon;

    public List<Transform> SpawnPoints;

    private Rigidbody2D body;
    private CanJump canJump;
    private LaunchSlime laucher;
    private Vector2 acel;

    public float groundSpeed = 10f;
    public float airSpeed = 5f;
    private float speed = 5f;
    public float jump = 5f;
    private bool throwing = false;

    public float InitialAngle = 45f;
    public float ArchAngle = 30f;
    public float step = 0.2f;

    private GameObject laucheCross;

    private float CurrentAngle;
    private Animator animator;

    public float DistanceToMother = 5.0f;

    public new void Start(){
        base.Start();
        body = GetComponent<Rigidbody2D>();
        canJump = GetComponentInChildren<CanJump>();
        laucher = GetComponentInChildren<LaunchSlime>();
        laucher.BabySlimePrefab = BabySlimePrefab;
        laucher.MotherSlime = this;
        laucheCross = laucher.GetComponentInChildren<SpriteRenderer>().gameObject;
        CurrentAngle = InitialAngle;
        animator = GetComponentInChildren<Animator>();
        CurrentNumberOfLifes = InitialNumberOfLifes;
    }

    // Update is called once per frame
    void Update()
    {
        if(controllable){
            float x = Input.GetAxisRaw("Horizontal") * 0;
            acel = new Vector2(x,  0);
            speed = canJump.can ? groundSpeed : airSpeed * 0.9f;
            if(  canJump.can && Input.GetButtonDown("Jump") && false){
                acel.y = 1;
                canJump.StartingJump();
                animator.SetTrigger("Jumping");
            }

            if( Input.GetButtonDown("SpawnSlime") && CurrentNumberOfLifes > 0 ){
                throwing = true;
            }
            if( Input.GetButtonUp("SpawnSlime") && CurrentNumberOfLifes > 0 ){
                throwing = false;
                //throw baby slime
                CurrentNumberOfLifes--;
                CurrentSon = laucher.launchBabySlime();
            }
            laucheCross.SetActive(throwing);
            if( throwing ){
                // Draw arrow and rotate
                CurrentAngle += step * Time.deltaTime;
                if( CurrentAngle > InitialAngle + ArchAngle || CurrentAngle < InitialAngle - ArchAngle ){
                    CurrentAngle = step > 0 ? InitialAngle + ArchAngle : InitialAngle - ArchAngle;
                    step *= -1;
                }
            }
        }


        Vector3 scale = transform.localScale;
        laucher.transform.rotation = Quaternion.Euler(0,0,CurrentAngle*Mathf.Sign(scale.x));
        if( body.velocity.normalized.x > 0 && transform.localScale.x < 0 ){
            scale.x = -scale.x;
        }
        else if( body.velocity.normalized.x < 0 && transform.localScale.x > 0 ){
            scale.x = -scale.x;
        }
        transform.localScale = scale;
        animator.SetFloat("VelocityY", body.velocity.y);
        animator.SetFloat("VelocityX", body.velocity.x);
        animator.SetFloat("SpeedX", Mathf.Abs(body.velocity.x));
        animator.SetBool("Walking", Mathf.Abs(body.velocity.x) > Mathf.Epsilon);
        
        if( CurrentSon && SpawnPoints.Count > 0 ){
            float dist = Vector2.Distance(CurrentSon.transform.position, SpawnPoints[0].position);
            if(dist < DistanceToMother){
                CurrentNumberOfLifes += InitialNumberOfLifes;
                transform.position = SpawnPoints[0].position;
                SpawnPoints[0].gameObject.SetActive(false);
                SpawnPoints.RemoveAt(0);
            }
        }

        if( !CurrentSon && CurrentNumberOfLifes == 0 && SpawnPoints.Count > 0 && controllable ){
            transform.position = SpawnPoints[0].position;
            SpawnPoints[0].gameObject.SetActive(false);
            CurrentNumberOfLifes = InitialNumberOfLifes;
            SpawnPoints.RemoveAt(0);
        }
        if( CurrentNumberOfLifes == 0 && SpawnPoints.Count == 0 && controllable ){
            GameOver();
        }
    }

    private void GameOver()
    {
        // TODO
        Debug.Log("GAME OVER");
    }

    void FixedUpdate() { 
        body.AddForce(acel * new Vector2(speed, jump));
    }
}
