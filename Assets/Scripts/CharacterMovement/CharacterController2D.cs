﻿using System;
using DG.Tweening;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

public class CharacterController2D : CharacterController
{
	[SerializeField] public float m_DashSpeed = 4.0f;
	[SerializeField] public float m_timeToPrepareDash = 0.1f;
	[SerializeField] public float m_DashDrag = 14.0f;
	[SerializeField] public float m_DragTime = 1.0f;
	[SerializeField] public float bounciness = 0.7f;
	[Range(0, .3f)] [SerializeField] public float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] public LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
	[SerializeField] public Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
	[SerializeField] public Transform m_CeilingCheck;							// A position marking where to check for ceilings
	[SerializeField] public float m_airControl;
	[SerializeField] public Collider2D m_WallCheck;
	[SerializeField] public float bounceThreshold = 7.0f;
	[SerializeField] public float bounceThresholdDiagonal = 10.0f;
	[SerializeField] public float m_normalFriction = 0.5f;
	[SerializeField] public LayerMask collisionLayers;
	[SerializeField] public float rayDist = 1.0f;
	[SerializeField] public Vector3 offset = new Vector3(0.0f, 1.0f, 0.0f);
	[SerializeField] public float horizontalSpeedCap = 20.0f;

	[Header("Jump")]
	public float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
	[Range(0.0f, 5.0f)] [SerializeField] public float m_jumpingDrag = 0.5f;
	[Range(0.0f, 5.0f)] [SerializeField] public float m_jumpingGravity = 1;
	[Range(0.0f, 5.0f)] [SerializeField] public float m_fallingDrag = 20;
	[Range(0.0f, 5.0f)] [SerializeField] public float m_fallingGravity = 2;

	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	public bool m_Grounded = true;            // Whether or not the player is grounded.
	public Rigidbody2D m_Rigidbody2D;
	public bool m_FacingRight = true;  // For determining which way the player is currently facing.
	public Vector3 m_Velocity = Vector3.zero;

	public Coroutine m_JumpCoroutine = null;
	
	public Vector2 m_LastVelocity;
	public Vector2 thirdFrameVelocity;
	public Vector2 fourthFrameVelocity;

	public bool m_Dashing;
	public bool m_PrepareDash;
	public float timeInDash;
	public float timeInPrepareDash;
	public bool canDash = true;
	public float newCollisionCD = 0.0f;
	public bool m_wallJumping = false;
	public Vector3 wallJumpVelocity = new Vector3(0.0f, 0.0f, 0.0f);
	public GameObject bounceParticleSystem;
	
	public bool inLava;
	public float maxHp = 100.0f;
	public float hpPerTouch = 2.0f;
	public float currHp = 0.0f;
	public float timeBetweenLifeTick = 0.2f;
	public float currLifeTick = 0.0f;
	public float hpInLavaPerTick = 10.0f;


	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	public CapsuleCollider2D _capsuleCollider2D;
	public Collisions collisionsDetection;
	public Animator animator;
	public TrailRenderer trailRenderer;
	public Material mat;

	public float shakeTime = 1.0f;
	public float intensity = 1.0f;
	public CameraShake cameraShake;

	[SerializeField] public AudioSource dashSound;
	[SerializeField] public AudioSource walkingSound;
	public bool walkingSoundPlaying = false;
	[SerializeField] public AudioSource bounceSound;
	
    public bool dieable = true;
	public bool exploded = false;
	public UIController uiController;
	public Vector2 prevMoveDir;
	public float prevMoveLastTime;
	public Vector2 thisFrameMoveDir;

	public FrostEffect frost;

    public void Awake()
	{
		_capsuleCollider2D = GetComponent<CapsuleCollider2D>();
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		animator = GetComponentInChildren<Animator>();
		trailRenderer = GetComponentInChildren<TrailRenderer>();
		collisionsDetection = GetComponent<Collisions>();
		frost = Camera.main.GetComponent<FrostEffect>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();
	}
    public void Start()
    {
		mat = GetComponentInChildren<SpriteRenderer>().material;
		cameraShake = Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CameraShake>();
		uiController = GameObject.FindGameObjectWithTag("UiCanvas").GetComponent<UIController>();
		mat.EnableKeyword("_HsvSaturation");
		mat.SetFloat("_HsvSaturation", 1.0f);
		currHp = maxHp;
	}

    private void Update()
    {
		if( (currHp == 0 || exploded) && dieable){
			Debug.Log("Slime Killed!");
			Destroy(gameObject);
			frost.FrostAmount = 0.0f;
			currHp = Mathf.Clamp(currHp, 0.0f, maxHp);
			uiController.bar = currHp / maxHp;
		}
		animator.SetBool("OnAirDown", false);
		animator.SetBool("OnAirUp", false);

		timeInDash += Time.deltaTime;
		currLifeTick += Time.deltaTime;
		prevMoveLastTime += Time.deltaTime;

		if (!m_Grounded)
        {
			if(m_Rigidbody2D.velocity.y > 0.0f)
            {
				animator.SetBool("OnAirUp", true);
			}
			else
            {
				animator.SetBool("OnAirDown", true);
			}
        }

		if (canDash)
			mat.SetFloat("_HsvSaturation", 1.0f);
		else
			mat.SetFloat("_HsvSaturation", 0.0f);

		if (inLava)
			currHp += hpInLavaPerTick;

		currHp = Mathf.Clamp(currHp, 0.0f, maxHp);
		uiController.bar = currHp / maxHp;
		frost.FrostAmount = 1 - currHp / maxHp;
	}

    private void CheckGroundAndBounce()
	{
		fourthFrameVelocity = thirdFrameVelocity;
		thirdFrameVelocity = m_LastVelocity;
		m_LastVelocity = m_Rigidbody2D.velocity;

		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		if(collisionsDetection.collisionDown.active) {
			m_Grounded = true;

			if (currLifeTick > timeBetweenLifeTick && !inLava)
			{
				currHp -= hpPerTouch;
				currLifeTick = 0.0f;
			}

			if (!wasGrounded)
				OnLandEvent.Invoke();
		}

		// Bounce Mechanic
		//Pick the direction where the velocity is the largest
		float maxDirectionDot = 0.0f;
		Vector2 vel = m_Rigidbody2D.velocity;
		if(fourthFrameVelocity.magnitude > vel.magnitude)
        {
			vel = fourthFrameVelocity;
        }
		if (thirdFrameVelocity.magnitude > vel.magnitude)
		{
			vel = thirdFrameVelocity;
		}
		if (m_LastVelocity.magnitude > vel.magnitude)
		{
			vel = m_LastVelocity;
		}

		Collisions.Direction maxDir = null;
		bool isDiagonal = false;
		for (int i = 0; i < collisionsDetection.directions.Length; i++)
        {
			Collisions.Direction dir = collisionsDetection.directions[i];
			float dirDot = Vector2.Dot(dir.dir, vel);
			if (dir.active && dirDot > maxDirectionDot)
            {
				maxDirectionDot = dirDot;
				maxDir = dir;
				isDiagonal = Mathf.Abs(dir.dir.x) > 0.0 && Mathf.Abs(dir.dir.y) > 0.0f;
			}
        }

		//bounce slime of the choosen direction
		newCollisionCD -= Time.deltaTime;
		float diificultyCoefficient = isDiagonal ? bounceThresholdDiagonal : bounceThreshold;
		if (maxDir != null && maxDirectionDot > diificultyCoefficient && newCollisionCD <= 0.0f)
        {
			if (Mathf.Abs(thisFrameMoveDir.x) < 0.1f && vel.y < 0.0f)
			{
				vel.x = 0.0f;
			}

			wallJumpVelocity = Vector3.Reflect(bounciness * vel, maxDir.normal);
			if (Mathf.Abs(vel.x) < 0.1f)
				wallJumpVelocity.x = 0.0f;
			else if (Mathf.Abs(vel.y) < 0.1f)
				wallJumpVelocity.y = 0.0f;

			Vector2 wallJumpDir = wallJumpVelocity.normalized;

			if (wallJumpDir.x > 0.3f)
				wallJumpDir.x = 1.0f;
			else if (wallJumpDir.x < -0.3f)
				wallJumpDir.x = -1.0f;

			if (wallJumpDir.y > 0.3f)
				wallJumpDir.y = 1.0f;
			else if (wallJumpDir.y < -0.3f)
				wallJumpDir.y = -1.0f;


			if (maxDir.dir != new Vector2(0.0f, -1.0f) && !inLava)
			{
				currHp -= hpPerTouch;
			}

			if(wallJumpDir.magnitude > 0.1f)
				wallJumpVelocity = wallJumpDir.normalized * wallJumpVelocity.magnitude;

			newCollisionCD = 0.1f;
			m_wallJumping = true;

			bounceSound.Play();

			if (Mathf.Abs(maxDir.dir.x) > 0.0f)
				animator.SetBool("HitWall", true);
		}
	}
	private IEnumerator reactivateCollisions()
    {
		yield return new WaitForSeconds(0.1f);
		collisionsDetection.enabled = true;
	}
	private IEnumerator ReenableWallTrigger()
	{
		yield return new WaitForSeconds(0.1f);
		m_WallCheck.enabled = true;
	}


	public virtual void Move(Vector2 move, bool jump, bool dash, Vector2 moveDir)
	{
		thisFrameMoveDir = moveDir;

		if (m_wallJumping)
			return;
		
		// If the input is moving the player right and the player is facing left...
		if (move.x > 0 && !m_FacingRight)
		{
			// ... flip the player.
			Flip();
		}
		// Otherwise if the input is moving the player left and the player is facing right...
		else if (move.x < 0 && m_FacingRight)
		{
			// ... flip the player.
			Flip();
		}
		
		//only control the player if grounded
		if (m_Grounded && !m_PrepareDash)
		{
			if (timeInDash > 0.3f + m_timeToPrepareDash)
				canDash = true;

			// Move the character by finding the target velocity
			Vector3 targetVelocity = new Vector2(move.x * 10f, m_Rigidbody2D.velocity.y);
			// And then smoothing it out and applying it to the character
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			if (Mathf.Abs(targetVelocity.x) < 0.01f)
				m_Rigidbody2D.velocity = new Vector2(0.0f, m_Rigidbody2D.velocity.y);

			if (m_Rigidbody2D.velocity.magnitude < 0.1f)
            {
				walkingSound.Stop();
				walkingSoundPlaying = false;
			}
			else if(!walkingSoundPlaying) {
				walkingSound.Play();
				walkingSoundPlaying = true;
			}

			if (Mathf.Abs(move.x) > 0.0f)
            {
				animator.SetBool("IsWalking", true);
			}
			else
            {
				animator.SetBool("IsWalking", false);
			}
		}
		else
		{
			walkingSound.Stop();
			walkingSoundPlaying = false;
			m_Rigidbody2D.AddForce(new Vector2(move.x * m_airControl, 0));
		}


		if (m_Grounded && jump)
		{
			animator.SetTrigger("StartJump");

			if (m_JumpCoroutine != null)
			{
				StopCoroutine(m_JumpCoroutine);
			}

			m_JumpCoroutine = StartCoroutine(JumpCoroutine());
		}

		if(dash && canDash)
        {
			canDash = false;
			m_PrepareDash = true;
			timeInDash = 0.0f;
			m_Rigidbody2D.gravityScale = 0;
			animator.SetBool("Dash", true);
			dashSound.Play();
		}

		if (moveDir.magnitude > 0.1f)
		{
			prevMoveDir = moveDir;
			prevMoveLastTime = 0.0f;
		}

		if(m_PrepareDash && timeInDash > m_timeToPrepareDash)
        {
			//m_Rigidbody2D.AddForce( move.normalized * m_JumpForce);
			Vector2 tmp = moveDir;
			if (tmp.magnitude < 0.1f && prevMoveLastTime < 0.2f)
				tmp = prevMoveDir;
			m_PrepareDash = false;
			// if no move, go the direction player is moving
			if (tmp.magnitude < 0.01f) {
				tmp = new Vector2(0.0f, 1.0f);
			}

			m_Rigidbody2D.velocity = tmp * m_DashSpeed;
			StartCoroutine(DashWait());
		}

		bool wasGrounded = m_Grounded;
		CheckGroundAndBounce();
		animator.SetBool("Grounded", m_Grounded);
	

		if(m_wallJumping)
        {
			StartCoroutine(Walljump());
		}
	}
	
	/**
	 * Esta coroutina pode parecer estúpida (mete um if no Update duh)
	 * Mas brevemente quero personalizar mais as frames iniciais do salto e
	 * acho que me vai dar muito jeito ter isto já separado numa Coroutine.
	 * Porque Coroutines são Máquinas de Estado :D
	 */
	IEnumerator JumpCoroutine()
	{
		// Add a vertical force to the slime.
		m_Grounded = false;
		m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
		yield return new WaitForFixedUpdate();

		// Going up....
		DOVirtual.Float(0, m_jumpingDrag, .8f, RigidbodyDrag);
		m_Rigidbody2D.gravityScale = m_jumpingGravity;
		yield return new WaitWhile(() => m_Rigidbody2D.velocity.y > 0);
		
		// The slime either reached the apex or hit the ceiling. Going down...
		m_Rigidbody2D.drag = m_fallingDrag;
		m_Rigidbody2D.gravityScale = m_fallingGravity;
	}

	public IEnumerator DashWait()
	{
		timeInDash = 0.0f;
		//interpolate drag over tiems
		StartCoroutine(interpolateDrag(m_DashDrag, 0, m_DragTime));

		trailRenderer.emitting = true;
		//GetComponent<BetterJumping>().enabled = false;
		m_Dashing = true;

		float time = 0.0f;
		while(time < 0.2f) {
			time += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}

		animator.SetBool("Dash", false);

		time = 0.0f;
		while (time < 0.1f)
		{
			time += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}

		trailRenderer.emitting = false;
		m_Rigidbody2D.gravityScale = m_fallingGravity;
		m_Rigidbody2D.drag = m_fallingDrag;
		//rb.gravityScale = 3;
		//GetComponent<BetterJumping>().enabled = true;
		m_Dashing = false;
	}
	IEnumerator interpolateDrag(float initialDrag, float finalDrag, float finalTime)
    {
		float time = 0.0f;
		while(time <= finalTime) {
			m_Rigidbody2D.drag = Mathf.Lerp(initialDrag, finalDrag, time / finalTime);
			time += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		m_Rigidbody2D.drag = Mathf.Lerp(initialDrag, finalDrag, time / finalTime);
	}

	IEnumerator Walljump()
    {
		Instantiate(bounceParticleSystem, transform.position, Quaternion.identity);
		//cameraStressReceiver.InduceStress(1.0f);
		if(cameraShake != null)
			cameraShake.ShakeCamera(intensity, shakeTime);
		m_Rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
		m_Rigidbody2D.gravityScale = 0;
		yield return new WaitForSeconds(.1f);
		m_Rigidbody2D.gravityScale = m_jumpingGravity;
		m_Rigidbody2D.velocity = wallJumpVelocity;
		canDash = true;
		m_wallJumping = false;
		animator.SetBool("HitWall", false);
	}

	void RigidbodyDrag(float x)
	{
		m_Rigidbody2D.drag = x;
	}

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
		if(collision.gameObject.layer == LayerMask.NameToLayer("Lava"))
        {
			inLava = true;
		}
    }

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Lava"))
		{
			inLava = false;
		}
	}

	public void Explode(){
		exploded = true;
	}
}