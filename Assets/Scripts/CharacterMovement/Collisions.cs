﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    public class Direction
    {
        public bool active = true;
        public Vector2 normal = new Vector3();
        public Vector2 dir;
        public string tag;
        public Direction(Vector3 dir) { this.dir = dir; }
    }

    public LayerMask collisionLayers;
    public float rayDist = 0.1f;
    public float rayDistDiagonal = 0.1f;
    public Vector2 offset = new Vector2(0.0f, 0.2f);
    private Rigidbody2D rb;

    [System.NonSerialized] public Direction[] directions = new Direction[8];
    [System.NonSerialized] public Direction collisionRight;
    [System.NonSerialized] public Direction collisionLeft;
    [System.NonSerialized] public Direction collisionDown;
    [System.NonSerialized] public Direction collisionTop;
    [System.NonSerialized] public Direction collisionUpRight;
    [System.NonSerialized] public Direction collisionUpLeft;
    [System.NonSerialized] public Direction collisionDownRight;
    [System.NonSerialized] public Direction collisionDownLeft;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        //right
        directions[0] = new Direction(new Vector2(1.0f, 0.0f).normalized);
        collisionRight = directions[0];
        //left
        directions[1] = new Direction(new Vector2(-1.0f, 0.0f).normalized);
        collisionLeft = directions[1];
        //down
        directions[2] = new Direction(new Vector2(0.0f, -1.0f).normalized);
        collisionDown = directions[2];
        //top
        directions[3] = new Direction(new Vector2(0.0f, 1.0f).normalized);
        collisionTop = directions[3];
        //Up right
        directions[4] = new Direction(new Vector2(1.0f, 1.0f).normalized);
        collisionUpRight = directions[4];
        //Up left
        directions[5] = new Direction(new Vector2(-1.0f, 1.0f).normalized);
        collisionUpLeft = directions[5];
        //Down Right
        directions[6] = new Direction(new Vector2(1.0f, -1.0f).normalized);
        collisionDownRight = directions[6];
        //Down Left
        directions[7] = new Direction(new Vector2(-1.0f, -1.0f).normalized);
        collisionDownLeft = directions[7];
    }

    // Update is called once per frame
    public void Update()
    {
        //Right
        for (int i = 0; i < 8; i++)
        {
            Direction dir = directions[i];
            float rayLength = 0.0f;
            if(Mathf.Abs(dir.dir.x) > 0.0f && Mathf.Abs(dir.dir.y) > 0.0f)
            {
                rayLength = rayDistDiagonal;
            }
            else
            {
                rayLength = rayDist;
            }

            RaycastHit2D raycastHit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + offset, dir.dir, rayLength, collisionLayers);

            if (raycastHit.collider != null)
            {
                dir.active = true;
                dir.normal = raycastHit.normal;
                dir.tag = raycastHit.transform.gameObject.tag;
                Debug.DrawRay(new Vector2(transform.position.x, transform.position.y) + offset, dir.dir * rayLength, Color.red);
                RecieveColideEvent re = raycastHit.transform.GetComponent<RecieveColideEvent>();
                if( re != null ){
                    re.EventAt(raycastHit.point + dir.dir * 0.3f, gameObject);
                }
            }
            else
            {
                Debug.DrawRay(new Vector2(transform.position.x, transform.position.y) + offset, dir.dir * rayLength, Color.green);
                dir.active = false;
            }
        }
    }
}
