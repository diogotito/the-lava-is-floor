﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLogic : MonoBehaviour
{
    [NonSerialized] public bool isAlive = true;
    
    /**
     * `Destroy(gameObject)` mete o gameObject numa fila para ser destruído no final do frame.
     * Como não o destrói imediatamente, `((bool) gameObject)` e `(gameObject != null)` dizem, e com razão,
     * que o objeto nativo ainda está vivo.
     * O Unity não nos diz se um objeto está condenado à destruição, temos de ser nós a manter essa informação.
     * Eu por acaso preciso disto para o RoomCamTrigger, mas acho que não devíamos precisar disto a longo prazo.
     * Vou tirar isto quando fizer algo mais sofisticado para celebrar a morte da slime.
     */
    public void Die()
    {
        isAlive = false;
        Destroy(gameObject);
    }
    
    public virtual void touchedExplosiveTile() {}
    public virtual void touchedTile() { }
}
