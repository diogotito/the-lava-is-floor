﻿using UnityEngine;

public partial class CharacterController2DKinematic
{

#if UNITY_EDITOR

    [Header("OnGUI Debugging Panel")]
    public bool onguiEnabled = true;
    
    // Window rectangle
    private Rect debuggingRect = new Rect(20.0f, 80.0f, 250.0f, 100.0f);

    private void OnGUI()
    {
        if (!onguiEnabled)
            return;
        
        void DebuggingWindow(int id)
        {
            ShowVariable("Velocity", this.physics.rb.velocity);
        }

        void ShowVariable(string label, object value)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(label + ": ",
                new GUIStyle
                    {alignment = TextAnchor.MiddleRight, normal = new GUIStyleState {textColor = Color.white}});
            GUILayout.Box(value.ToString());
            GUILayout.EndHorizontal();
        }

        debuggingRect = GUILayout.Window(0, debuggingRect, DebuggingWindow, "Debugging");
    }

#endif
    
}