using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftBody : MonoBehaviour
{
    [System.Serializable]
    public class Point2D
    {
        public Transform transform;
        public Rigidbody2D rb2d; //rigidbody 2D
        public Vector2 initialLocalPosition; //initial local position of point

        public Point2D(Transform transform, Rigidbody2D rb2d)
        {
            this.transform = transform;
            initialLocalPosition = transform.localPosition;
            this.rb2d = rb2d;
        }

        public Point2D() { }
    }

    [System.Serializable]
    public class Spring
    {
        public int p1Index, p2Index; // points indexes
        public float length; // rest length
        public Vector2 normal; // normal vector

        public Spring(int p1Index, int p2Index, float length, Vector2 normal )
        {
            this.p1Index = p1Index;
            this.p2Index = p2Index;
            this.length = length;
            this.normal = normal;
        }

        public Spring() { }
    }

    [SerializeField] public List<Point2D> points = new List<Point2D>();
    [SerializeField] public List<Spring> springs = new List<Spring>();
    [SerializeField] public float gravity = -9.8f;
    [SerializeField] public float mass = 1.0f;

    public float pressure = 50.0f;
    public float finalPressure = 0.0f;
    public float springConstantElasticity = 1200.0f;   // spring constant
    public float springDamping = 10.0f;				 // spring damping constant
    public int extraPoints = 1;
    public float centerPointConstantElasticity = 1200.0f;

    public Point2D centerPoint;
    [SerializeField] public List<Spring> centerPointSprings = new List<Spring>();
    public float constraintX = 0.1f;
    public float constraintY = 0.1f;
    [SerializeField] public List<Transform> constraints = new List<Transform>();

    // Start is called before the first frame update
    public void buildMesh()
    {
        /*
        int numPoints = transform.childCount;

        points = new List<Point2D>(new Point2D[numPointsconstraints
        
        
        //add the points to the list
        for (int i = 0; i < numPoints; i++) {
            Transform point = transform.GetChild(i);
            points[i] = new Point2D(point,
                                    point.GetComponent<Rigidbody2D>());
        }

        List<Point2D> extendedPoints = new List<Point2D>();

        //create extra points on edges for stability
        for (int j = 0; j < points.Count; j++) { 
            extendedPoints.Add(points[j]);

            Vector3 p1p2 = points[(j+1) % points.Count].transform.position - points[j].transform.position;
            float lengthp1p2 = p1p2.magnitude;
            Vector3 p1p2dir = p1p2.normalized;

            for (int i = 0; i < extraPoints; i++)
            {
                extendedPoints.Add(createNewPoint(points[j].transform.position + p1p2dir * lengthp1p2 / (extraPoints + 1) * (i + 1), transform));
            }
        }

        points = extendedPoints;
        */

        //create the springs
        springs = new List<Spring>(new Spring[points.Count]);
        for (int i = 0; i < points.Count; i++) {
            float length = Vector2.Distance(points[i].transform.position, 
                                            points[(i + 1) % points.Count].transform.position);
            springs[i] = new Spring(i, (i+1) % points.Count, length, new Vector2());
        }

        
        //calculate center point
        /*
        Vector3 centerOfMass = new Vector3();
        for (int i = 0; i < points.Count; i++)
        {
            centerOfMass += points[i].transform.position;
        }
        centerOfMass /= points.Count;

        GameObject newPoint = new GameObject();
        newPoint.transform.parent = transform;
        newPoint.transform.position = centerOfMass;

        Rigidbody2D rb = newPoint.AddComponent<Rigidbody2D>();
        rb.mass = 1.0f;
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rb.isKinematic = true;

        centerPoint = new Point2D(newPoint.transform, rb);
        */

        //add an edge to the center point
        for (int i = 0; i < points.Count; i++)
        {
            float length = Vector2.Distance(centerPoint.transform.position,
                                            points[i].transform.position);
            centerPointSprings.Add(new Spring(points.Count, i, length, new Vector2()));
        }

    }

    public Point2D createNewPoint(Vector3 position, Transform parent)
    {
        GameObject newPoint = new GameObject();
        newPoint.transform.parent = transform;
        newPoint.transform.position = position;

        //set rigidbody
        Rigidbody2D rb = newPoint.AddComponent<Rigidbody2D>();
        rb.mass = 1.0f;
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;

        //set circle collider
        CircleCollider2D circleCollider = newPoint.AddComponent<CircleCollider2D>();
        circleCollider.radius = 0.1f;

        //set gizmos drawer
        CircleGizmos circleGizmos = newPoint.AddComponent<CircleGizmos>();
        circleGizmos.radius = circleGizmos.radius / 2.0f;

        return new Point2D(newPoint.transform, rb);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //apply gravity
        /*
        for (int i = 0; i < points.Count; i++)
        {
            if (pressure > finalPressure)
                points[i].f = new Vector2(0.0f, mass * gravity);
            else
                points[i].f = new Vector2(0.0f, 0.0f);
        }
        */

        //apply spring linear force
        for (int i = 0; i < springs.Count; i++)
        {
            // get positions of spring start & end points
            Spring spring = springs[i];
            Point2D p1 = points[spring.p1Index];
            Point2D p2 = points[spring.p2Index];
            float r12d = Vector2.Distance(p1.transform.position, p2.transform.position);
            if (r12d != 0) // start = end?
            {
                Vector2 vel = p1.rb2d.velocity - p2.rb2d.velocity;
                // calculate force value
                float f = (r12d - spring.length) * springConstantElasticity +
                          (vel.x * (p1.transform.position.x - p2.transform.position.x) +
                          vel.y * (p1.transform.position.y - p2.transform.position.y)) * springDamping / r12d;
                // force vector
                Vector2 finalForce;
                finalForce = (p1.transform.position - p2.transform.position).normalized * f;

                p1.rb2d.AddForce(-finalForce, ForceMode2D.Force);
                p2.rb2d.AddForce(finalForce, ForceMode2D.Force);
            }

            spring.normal.x = (p1.transform.position.y - p2.transform.position.y) / r12d;
            spring.normal.y = -(p1.transform.position.x - p2.transform.position.x) / r12d;
        }

        //apply center force
        for(int i = 0; i < centerPointSprings.Count; i++)
        {
            Point2D p1 = centerPoint;
            Point2D p2 = points[centerPointSprings[i].p2Index];
            float r12d = Vector2.Distance(p1.transform.position, p2.transform.position);

            Vector2 vel = p1.rb2d.velocity - p2.rb2d.velocity;
            // calculate force value
            float f = (r12d - centerPointSprings[i].length) * springConstantElasticity +
                      (vel.x * (p1.transform.position.x - p2.transform.position.x) +
                      vel.y * (p1.transform.position.y - p2.transform.position.y)) * springDamping / r12d;
            // force vector
            Vector2 finalForce;
            finalForce = (p1.transform.position - p2.transform.position).normalized * f;

            p2.rb2d.AddForce(finalForce, ForceMode2D.Force);
        }


        //Apply pressure force
        
        //Calculate volume of slime
        float volume = 0.0f;
        for (int i = 0; i < springs.Count; i++)
        {
            Vector2 p1 = points[springs[i].p1Index].transform.position;
            Vector2 p2 = points[springs[i].p2Index].transform.position;
            float r12d = Vector2.Distance(p1, p2);
            volume += 0.5f * Mathf.Abs(p1.x - p2.x) * Mathf.Abs(springs[i].normal.x) * (r12d);
        }

       
        for (int i = 0; i < springs.Count; i++)
        {
            Spring spring = springs[i];
            Point2D p1 = points[spring.p1Index];
            Point2D p2 = points[spring.p2Index];
            float r12d = Vector2.Distance(p1.transform.position, p2.transform.position);
            float pressurev = r12d * pressure * (1.0f / volume);

            p1.rb2d.AddForce(new Vector2(pressurev * spring.normal.x, pressurev * spring.normal.y), ForceMode2D.Force);
            p2.rb2d.AddForce(new Vector2(pressurev * spring.normal.x, pressurev * spring.normal.y), ForceMode2D.Force);
        }
    }

    //APPLY RESTRICTION AthFTER PHYSICS CALCULATION AND UPDATE SLIME SHAPE
    private void LateUpdate()
    {
        float scaledConstraintX = constraintX * Mathf.Min(Mathf.Abs(transform.GetComponent<Rigidbody2D>().velocity.x + 1.0f), 3.0f);
        float scaledConstraintY = constraintY * Mathf.Min(Mathf.Abs(transform.GetComponent<Rigidbody2D>().velocity.y + 1.0f), 3.0f);

        for (int i = 0; i < points.Count; i++)
        {
            Transform constraint = constraints[i];
            Transform point = points[i].transform;

            if (Mathf.Abs(constraint.position.x - point.position.x) > scaledConstraintX)
            {
                point.position = new Vector2(constraint.position.x, point.position.y) + new Vector2(point.position.x - constraint.position.x, 0.0f).normalized * scaledConstraintX;
            }

            if (Mathf.Abs(constraint.position.y - point.position.y) > scaledConstraintY)
            {
                point.position = new Vector2(point.position.x, constraint.position.y) + new Vector2(0.0f, point.position.y - constraint.position.y).normalized * scaledConstraintY;
            }
        }

        transform.parent.GetComponent<SlimeShapeController>().UpdateVertices();
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < springs.Count; i++) {
            Point2D p1 = points[springs[i].p1Index];
            Point2D p2 = points[springs[i].p2Index];
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(p1.transform.position, p2.transform.position);

            Gizmos.color = Color.white;
            Gizmos.DrawLine(p1.transform.position + 
                            (p2.transform.position - p1.transform.position) / 2.0f, 
                            p1.transform.position + (p2.transform.position - p1.transform.position) / 2.0f 
                            + new Vector3(springs[i].normal.x * 0.2f, springs[i].normal.y * 0.2f, 0.0f));
        }

        for (int i = 0; i < centerPointSprings.Count; i++)
        {
            Point2D p1 = centerPoint;
            Point2D p2 = points[centerPointSprings[i].p2Index];
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(p1.transform.position, p2.transform.position);
        }

        for(int i = 0; i < constraints.Count; i++)
        {
            Gizmos.color = Color.red;
            float scaledConstraintX = constraintX * Mathf.Min(Mathf.Abs(transform.GetComponent<Rigidbody2D>().velocity.x + 1.0f), 3.0f);
            float scaledConstraintY = constraintY * Mathf.Min(Mathf.Abs(transform.GetComponent<Rigidbody2D>().velocity.y + 1.0f), 3.0f);
            Gizmos.DrawWireCube(constraints[i].position, new Vector3(scaledConstraintX * 2, scaledConstraintY * 2));
        }
    }
}
