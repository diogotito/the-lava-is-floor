
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Tilemaps;
using static Utils;

public class DestroyablePlatforms : RecieveColideEvent
{
    public enum PlatformState { Stable, SemiDestroyed, Destroyed }
    public float[] crystalBrightnesses = {2.0f, 0.2f, 0.1f};

    public Transform stablePlatform;
    public Transform semiDestroyedPlatform;
    public Transform topSpikes;
    public Transform bottomSpikes;
    public Transform crystal;
    public Light2D crystalLight; 
    public Transform collisionParticleSystemPrefab;
    public Transform explosionTransform;
    public float cooldownForCollision = 0.1f;
    public float reformDelay = 0.5f;
    public float timeToReform = 0.1f;
    public float timeToAbsorbParticles = 0.4f;
    public float explosionForceMultiplier = 1.0f;
    public float explosionForceTime = 0.2f;
    public AnimationCurve explosionForceOverTime;
    public bool onlySemiDestroyed = false;

    private SpriteRenderer stablePlatformSpriteRenderer;
    private SpriteRenderer semiDestroyedPlatformSpriteRenderer;
    private Animator explosionAnimator;

    public PlatformState platformState = PlatformState.Stable;
    private float cooldown = 0.0f;
    public float timeInReforming = 0.0f;
    private Collider2D collider;
    private Material crystalMat;

    private Transform stableCollisionParticleSystem;
    private Transform semiDestroyedCollisionParticleSystem;

    //Coroutines
    private Coroutine stablePlatformCoroutine;
    private Coroutine semiDestroyedCoroutine;
    private IEnumerator stablePlatformAbsorptionRotine;
    private IEnumerator semiDestroyedAbsorptionRotine;
    private Coroutine crystalCoroutine;
    private Coroutine crystalLightCoroutine;
    public Vector2 platformCollisionCenter;

    private void Start()
    {
        collider = GetComponent<Collider2D>();
        crystalMat = crystal.GetComponent<SpriteRenderer>().material;
        stablePlatformSpriteRenderer = stablePlatform.GetComponent<SpriteRenderer>();
        semiDestroyedPlatformSpriteRenderer = semiDestroyedPlatform.GetComponent<SpriteRenderer>();
        explosionAnimator = explosionTransform.GetComponent<Animator>();
        LeanTween.init(1000, 1000);

        if(onlySemiDestroyed)
            switchState(PlatformState.SemiDestroyed, null);
    }

    public void Update()
    {
        cooldown -= Time.deltaTime;
        timeInReforming -= Time.deltaTime;

        if (platformState == PlatformState.SemiDestroyed && timeInReforming <= 0.0f && !onlySemiDestroyed)
        {
            switchState(PlatformState.Stable, null);
        }
        else if (platformState == PlatformState.Destroyed && timeInReforming <= (reformDelay + timeToReform))
        {
            switchState(PlatformState.SemiDestroyed, null);
        }
    }

    public override void EventAt(Vector3Int gridPos, TileBase cell, GameObject who)
    {
        if (who.TryGetComponent(out CharacterController2DKinematic cc2d) && cooldown <= 0.0f)
        {
            switchState(++platformState, cc2d);
            cooldown = cooldownForCollision;
        }
    }

    //cc2d parameter is null when the state swich isn't caused by the player
    private void switchState(PlatformState newState, CharacterController2DKinematic cc2d)
    {

        if (newState == PlatformState.Stable)
        {
            stablePlatform.gameObject.SetActive(true);
            collider.enabled = true;
            crystalMat.SetFloat("_HsvBright", crystalBrightnesses[0]);
            crystalLight.intensity = crystalBrightnesses[0];
        }
        else if (newState == PlatformState.SemiDestroyed)
        {
            //activate the collider
            collider.enabled = true;

            //reactivate spikes
            if(topSpikes)
                topSpikes.gameObject.SetActive(true);
            if(bottomSpikes)
                bottomSpikes.gameObject.SetActive(true);

            //make platform transparent
            stablePlatformSpriteRenderer.color = new Color(stablePlatformSpriteRenderer.color.r,
                                                            stablePlatformSpriteRenderer.color.g,
                                                            stablePlatformSpriteRenderer.color.b,
                                                            0.0f);
            if (!onlySemiDestroyed)
            {
                //start reforming the platform
                if (stablePlatformCoroutine != null)
                    StopCoroutine(stablePlatformCoroutine);

                stablePlatformCoroutine = StartCoroutine(interpolatePlatformOpacity(1.0f, reformDelay, timeToReform, stablePlatformSpriteRenderer));
                timeInReforming = timeToReform + reformDelay;

                //particle animation
                if (cc2d != null)
                {
                    stableCollisionParticleSystem = Instantiate(collisionParticleSystemPrefab, transform.position, Quaternion.identity);
            
                    if (stablePlatformAbsorptionRotine != null)
                        StopCoroutine(stablePlatformAbsorptionRotine);

                    ParticlesAttractorPlatforms particleAttractor = stableCollisionParticleSystem.GetComponent<ParticlesAttractorPlatforms>();
                    particleAttractor.target = transform;
                    particleAttractor.timeToTarget = timeToAbsorbParticles;

                
                    stablePlatformAbsorptionRotine = WaitAndDo((reformDelay + timeToReform) - timeToAbsorbParticles,
                                                        () => {
                                                            particleAttractor.enabled = true;
                                                        });
                    StartCoroutine(stablePlatformAbsorptionRotine);


                    ParticleSystem particleSystem = stableCollisionParticleSystem.GetComponent<ParticleSystem>();
                    StartCoroutine(WaitAndDo(0.2f,
                                            () => {
                                                ParticleSystem.MainModule main = particleSystem.main;
                                                main.gravityModifierMultiplier = 0;
                                                freezeParticles(particleSystem);
                                            }));
                }

                //crystal color animation
                if (crystalCoroutine != null)
                    StopCoroutine(crystalCoroutine);

                crystalMat.SetFloat("_HsvBright", crystalBrightnesses[1]);
                crystalCoroutine = StartCoroutine(interpolateCrystalBrightness(crystalBrightnesses[0], reformDelay, timeToReform));

                //crystal light animation
                if (crystalLightCoroutine != null)
                    StopCoroutine(crystalLightCoroutine);

                crystalLight.intensity = crystalBrightnesses[1];
                crystalLightCoroutine = StartCoroutine(interpolateCrystalLightIntensity(2.0f, reformDelay, timeToReform));

            }
        }
        else if(newState == PlatformState.Destroyed)
        {
            //deactivate the collider
            collider.enabled = false;

            //deactivate spikes
            if (topSpikes)
                topSpikes.gameObject.SetActive(false);
            if (topSpikes)
                bottomSpikes.gameObject.SetActive(false);

            //make platforms transparents
            stablePlatformSpriteRenderer.color = new Color(stablePlatformSpriteRenderer.color.r,
                                                           stablePlatformSpriteRenderer.color.g,
                                                           stablePlatformSpriteRenderer.color.b,
                                                           0.0f);

            semiDestroyedPlatformSpriteRenderer.color = new Color(semiDestroyedPlatformSpriteRenderer.color.r,
                                                                  semiDestroyedPlatformSpriteRenderer.color.g,
                                                                  semiDestroyedPlatformSpriteRenderer.color.b,
                                                                  0.0f);

            //start reforming the platform
            if (stablePlatformCoroutine != null)
                StopCoroutine(stablePlatformCoroutine);

            if (semiDestroyedCoroutine != null)
                StopCoroutine(semiDestroyedCoroutine);

            semiDestroyedCoroutine = StartCoroutine(interpolatePlatformOpacity(1.0f, reformDelay, timeToReform, semiDestroyedPlatformSpriteRenderer));
            timeInReforming = 2.0f * (timeToReform + reformDelay);

            //Deactivate stable platform particle attractor
            ParticlesAttractorPlatforms stableParticleAttractor = null;
            if (stableCollisionParticleSystem) { 
                stableParticleAttractor = stableCollisionParticleSystem.GetComponent<ParticlesAttractorPlatforms>();
                stableParticleAttractor.enabled = false;
            }

            //particle animation
            if (cc2d != null)
            {
                semiDestroyedCollisionParticleSystem = Instantiate(collisionParticleSystemPrefab, transform.position, Quaternion.identity);

                if (semiDestroyedAbsorptionRotine != null)
                    StopCoroutine(semiDestroyedAbsorptionRotine);

                ParticlesAttractorPlatforms particleAttractor = semiDestroyedCollisionParticleSystem.GetComponent<ParticlesAttractorPlatforms>();
                particleAttractor.target = transform;
                particleAttractor.timeToTarget = timeToAbsorbParticles;
                semiDestroyedAbsorptionRotine = WaitAndDo((reformDelay + timeToReform) - timeToAbsorbParticles,
                                                            () => {
                                                                particleAttractor.enabled = true;
                                                            });
                StartCoroutine(semiDestroyedAbsorptionRotine);

                ParticleSystem particleSystem = semiDestroyedCollisionParticleSystem.GetComponent<ParticleSystem>();
                StartCoroutine(WaitAndDo(0.2f,
                                        () => {
                                            ParticleSystem.MainModule main = particleSystem.main;
                                            main.gravityModifierMultiplier = 0;
                                            freezeParticles(particleSystem);
                                        }));
            }

            //Deactivate and restart absortpion animation for the stable platform
            if(!onlySemiDestroyed) { 
                if (stablePlatformAbsorptionRotine != null)
                    StopCoroutine(stablePlatformAbsorptionRotine);

                stablePlatformAbsorptionRotine = WaitAndDo(2.0f * ((reformDelay + timeToReform) - timeToAbsorbParticles) + timeToReform,
                                                            () => {
                                                                if(stableParticleAttractor)
                                                                    stableParticleAttractor.enabled = true;
                                                            });
                StartCoroutine(stablePlatformAbsorptionRotine);
            }

            //crystal color animation
            if (crystalCoroutine != null)
                StopCoroutine(crystalCoroutine);

            crystalMat.SetFloat("_HsvBright", crystalBrightnesses[2]);
            crystalCoroutine = StartCoroutine(interpolateCrystalBrightness(crystalBrightnesses[1], reformDelay, timeToReform));

            //crystal light animation
            if (crystalLightCoroutine != null)
                StopCoroutine(crystalLightCoroutine);

            crystalLight.intensity = crystalBrightnesses[2];
            crystalLightCoroutine = StartCoroutine(interpolateCrystalLightIntensity(crystalBrightnesses[1], reformDelay, timeToReform));

            //Trigger explosion
            explosionAnimator.SetTrigger("Explode");
        }
        else
        {
            Debug.Log("Unknown State");
            return;
        }


        platformState = newState;
    }
    private IEnumerator interpolatePlatformOpacity(float finalOpacity, float delay, float timeForInterpolation, SpriteRenderer platformSprite)
    {
        yield return new WaitForSeconds(delay);

        float timeInInterpolation = 0.0f;
        float originalOpacity = platformSprite.color.a;
        while (timeInInterpolation < timeForInterpolation)
        {
            timeInInterpolation += Time.deltaTime;
            float newOpacity = Mathf.Lerp(originalOpacity, finalOpacity, timeInInterpolation / timeForInterpolation);

            platformSprite.color = new Color(platformSprite.color.r, 
                                             platformSprite.color.g, 
                                             platformSprite.color.b, 
                                             newOpacity);

            yield return null;
        }
    }

    private IEnumerator interpolateCrystalBrightness(float finalBrightness, float delay, float timeForInterpolation)
    {
        yield return new WaitForSeconds(delay);
        float timeInInterpolation = 0.0f;
        float originalBrightness = crystalMat.GetFloat("_HsvBright");
        while (timeInInterpolation < timeForInterpolation)
        {
            timeInInterpolation += Time.deltaTime;
            float newBrightness = Mathf.Lerp(originalBrightness, finalBrightness, timeInInterpolation / timeForInterpolation);

            crystalMat.SetFloat("_HsvBright", newBrightness);

            yield return null;
        }
    }

    private IEnumerator interpolateCrystalLightIntensity(float finalIntensity, float delay, float timeForInterpolation)
    {
        yield return new WaitForSeconds(delay);
        float timeInInterpolation = 0.0f;
        float originalIntensity = crystalLight.intensity;
        while (timeInInterpolation < timeForInterpolation)
        {
            timeInInterpolation += Time.deltaTime;
            float newIntensity = Mathf.Lerp(originalIntensity, finalIntensity, timeInInterpolation / timeForInterpolation);

            crystalLight.intensity = newIntensity;

            yield return null;
        }
    }

    private void freezeParticles(ParticleSystem ps)
    {
        ParticleSystem.Particle[] m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
        int numParticlesAlive = ps.GetParticles(m_Particles);

        for (int i = 0; i < numParticlesAlive; i++)
        {
            m_Particles[i].velocity *= 0.2f;
        }

        ps.SetParticles(m_Particles, numParticlesAlive);
    }

    public Vector2 getPlayerForceDirection(Vector2 collisionPoint)
    {
        Vector2 impulseDirection = new Vector2();
        if (transform.position.x - collisionPoint.x > platformCollisionCenter.x / 2.0f)
            impulseDirection.x = -1.0f;
        else if(transform.position.x - collisionPoint.x < -platformCollisionCenter.x / 2.0f)
            impulseDirection.x = 1.0f;
        else
            impulseDirection.x = 0.0f;

        if (transform.position.y - collisionPoint.y > platformCollisionCenter.y / 2.0f)
            impulseDirection.y = -1.0f;
        else if (transform.position.y - collisionPoint.y < - platformCollisionCenter.y / 2.0f)
            impulseDirection.y = 1.0f;
        else
            impulseDirection.y = 0.0f;

        Debug.Log(impulseDirection);
        return impulseDirection;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, platformCollisionCenter);
    }
}
