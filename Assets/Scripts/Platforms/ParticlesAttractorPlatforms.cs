using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesAttractorPlatforms : MonoBehaviour
{
	ParticleSystem ps;
	ParticleSystem.Particle[] m_Particles;
	public Transform target;
	public float timeToTarget = 0.2f;
	int numParticlesAlive;

	Vector3[] particleOriginalPositions;

	private float time = 0.0f;
	void Start()
	{
		ps = GetComponent<ParticleSystem>();
		if (!GetComponent<Transform>())
		{
			GetComponent<Transform>();
		}
	}
	void Update()
	{
		m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
		numParticlesAlive = ps.GetParticles(m_Particles);
		time += Time.deltaTime;
		int numParticlesDestroyed = 0;

		if(particleOriginalPositions == null)
        {
			particleOriginalPositions = new Vector3[numParticlesAlive];
			for (int i = 0; i < numParticlesAlive; i++)
            {
				particleOriginalPositions[i] = m_Particles[i].position;
			}
		}

		for (int i = 0; i < numParticlesAlive; i++)
		{
			m_Particles[i].position = Vector3.Lerp(particleOriginalPositions[i], target.position, time / timeToTarget);
			float particleDistToTarget = Vector3.Distance(m_Particles[i].position, target.position);

			m_Particles[i].startColor = new Color(m_Particles[i].startColor.r / 255.0f,
												  m_Particles[i].startColor.g / 255.0f,
												  m_Particles[i].startColor.b / 255.0f,
												  Mathf.Min(particleDistToTarget / 4.0f, m_Particles[i].startColor.a / 255.0f));

			if (Vector3.Distance(m_Particles[i].position, target.position) < 0.01f) {
				m_Particles[i].remainingLifetime = 0.0f;
				numParticlesDestroyed++;
			}
		}
		ps.SetParticles(m_Particles, numParticlesAlive);

		if (numParticlesDestroyed == numParticlesAlive)
			Destroy(gameObject);
	}
}
