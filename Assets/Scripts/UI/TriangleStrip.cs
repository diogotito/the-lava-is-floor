﻿using System;
using System.Collections;
using UnityEngine;

namespace UI
{
    /**
     * | Redimensiona e centra linha de triângulos no fundo dos "cortinados" das transições para que fique simétrica. |
     * | [ExecuteAlways] é para os triângulos serem ajustados mesmo fora de Play Mode em tempo real.                  |
     * \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
     */
    [ExecuteAlways]
    public partial class TriangleStrip : MonoBehaviour
    {
#if UNITY_EDITOR
        public RectTransform rect;
        public RectTransform parentRect;
        public float triangleWidth = -1;

        private IEnumerator Start()
        {
            yield return null;
            CalculateWidth();
        }

        void Update()
        {
            if (!Application.IsPlaying(gameObject))
            {
                CalculateWidth();
            }
        }

        private void CalculateWidth()
        {
            if (triangleWidth < 0)
            {
                Debug.LogError("plz carrega no meu botao");
                return;
            }


            int triangles = Mathf.CeilToInt(Screen.width / triangleWidth);

            float remainder = triangles * triangleWidth - Screen.width;

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, triangles * triangleWidth);
        }
#endif
    }
}