﻿using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private HeatBarSlider _heatBar;

    [SerializeField] public float bar = 1.0f;

    public GameObject youWin;

    // Singleton
    public static UIController UI = null;
    
    private void Awake()
    {
        // Singleton
        if (UI == null)
        {
            UI = this;
        }
        else
        {
            Debug.LogFormat(
                "<b>[{0}]</b> Já há um UIController nesta cena. Estou a mais. Adeus.",
                name
            );
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        //_heatBar.SetSlider(bar -= 0.05f * Time.deltaTime);
        _heatBar.SetSlider(bar);
    }

    public void Win()
    {
        youWin.SetActive(true);
    }

}
