﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RecieveColideEvent : MonoBehaviour
{
    protected Tilemap m_Tilemap;
    protected Grid m_Grid;

    // Start is called before the first frame update
    void Start()
    {
        m_Tilemap = GetComponent<Tilemap>();
        m_Grid = GetComponentInParent<Grid>();
    }

    internal void EventAt(Vector2 point, GameObject who)
    {
        TileBase cell = null;
        Vector3Int gridPoint = new Vector3Int();
        if (m_Grid != null && m_Tilemap != null) {
            gridPoint = m_Grid.WorldToCell(point);
            cell = m_Tilemap.GetTile(gridPoint);
        }
        EventAt(gridPoint, cell, who);
    }

    //pos - position of the collision in the tilemap, empty vector when colliding with an object not in the tilemap
    //TileBase cell - the tile with which the player collided. If not part of tilemap, null is passed
    virtual public void EventAt(Vector3Int pos, TileBase cell, GameObject who){}
}
