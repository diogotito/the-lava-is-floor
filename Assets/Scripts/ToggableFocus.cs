﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class ToggableFocus : MonoBehaviour
{
    public List<Controllable> GameObjects;
    private CinemachineBrain cinematic;

    public bool ToogablePlayer = false;

    public CinemachineVirtualCamera fallbackCam;

    private int i = -1;
    private Controllable active;

    void Awake()
    {
        cinematic = this.GetComponent<CinemachineBrain>();
        GameObjects = new List<Controllable>();
    }

    private void Start()
    {
        if (fallbackCam == null)
        {
            GameObject fallbackCamGO = GameObject.FindGameObjectWithTag("FallbackCam");
            if (fallbackCamGO == null)
                Debug.LogError("NÃO ENCONTREI CAMERA PARA FALLBACK!");
            else
                fallbackCam = fallbackCamGO.GetComponent<CinemachineVirtualCamera>();
        }
    }

    // Update is called once per frame

    void Update()
    {
        if( !GameObjects.Contains(active) ){
            i=-1;
            if(fallbackCam){
                fallbackCam.Follow = null;
            }
        }
        
        // No inicio (i == -1) mete i = 0 e Activate(0). .Follow é whatever
        if( !active && GameObjects.Count > 0 && i == -1 ){
            i = 0;
            Activate(i);
        }
        if( Input.GetButtonDown("ChangeCharacter") && ToogablePlayer )
        {
            i = (i + 1) % GameObjects.Count;
            Activate(i);
        }

        bool anyActive = false;
        foreach(Controllable o in GameObjects){
            anyActive = o.controllable;
            if( anyActive ){
                break;
            }
        }
        if( !anyActive && GameObjects.Count > 0 ){
            i = 0; 
            Activate(i);
        }
    }

    private void Activate(int i)
    {
        for (int j = 0; j < GameObjects.Count; j++)
        {
            GameObjects[j].setControllable(false);
        }
        active = GameObjects[i];
        active.setControllable(true);
        if( fallbackCam ){
            fallbackCam.Follow = GameObjects[i].gameObject.transform;
        }
    }

    public void SetActive(GameObject go){
        Controllable controllable = go.GetComponent<Controllable>();
        if( controllable && controllable != active ){
            int i = GameObjects.IndexOf(controllable);
            if( i != -1 ){
                Activate(i);
            }
        } 
    }

}
