﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LaunchTutorial : MonoBehaviour
{
    public int TUTORIAL_INDEX = 1;
    public Animator curtain;
    
    public void GoToTutorial()
    {
        SceneManager.LoadScene(TUTORIAL_INDEX);
    }

    public void GoToNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void StartTransitionOut()
    {
        curtain.SetTrigger("TransitionOut");
        Debug.Log("TRANSITION OUT");
    }
}
