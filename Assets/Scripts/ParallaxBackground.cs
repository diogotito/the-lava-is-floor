using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] public bool affectedByTransitions = true;
    [SerializeField] public Vector2 parallaxMultiplier = new Vector2(0.5f, 0.5f);

    private Transform cameraTransform;
    private Cinemachine.CinemachineBrain cinemachineBrain;
    private Vector3 lastCameraPos;

    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = Camera.main.transform;
        cinemachineBrain = cameraTransform.GetComponent<Cinemachine.CinemachineBrain>();
        lastCameraPos = cameraTransform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastCameraPos;

        if (cinemachineBrain.IsBlending && affectedByTransitions) { 
            transform.position += new Vector3(deltaMovement.x * parallaxMultiplier.x, deltaMovement.y * parallaxMultiplier.y, 0.0f);

        }
        else if(cinemachineBrain.IsBlending && !affectedByTransitions)
        {

        }
        else
        {
            transform.position += new Vector3(deltaMovement.x * parallaxMultiplier.x, deltaMovement.y * parallaxMultiplier.y, 0.0f);
        }

        lastCameraPos = cameraTransform.position;
    }
}
