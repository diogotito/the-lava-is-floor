﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanJump : MonoBehaviour
{
    internal bool can;
    private void OnTriggerEnter2D(Collider2D other) {
        if( LayerMask.LayerToName(other.gameObject.layer) != "Ground" ) return;
        can = true;
    }

    private void OnTriggerExit2D(Collider2D other) {
        if( LayerMask.LayerToName(other.gameObject.layer) != "Ground" ) return;
        can = false;
    }

    private void OnTriggerStay2D(Collider2D other) {
        if( LayerMask.LayerToName(other.gameObject.layer) != "Ground" ) return;
        can = true;
    }

    internal void StartingJump()
    {
        can = false;
    }
}
