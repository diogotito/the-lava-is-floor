﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class OneUseTile : RecieveColideEvent
{
    public override void EventAt(Vector3Int pos, TileBase cell, GameObject who)
    {
        StartCoroutine(ClearTile(pos, 0.3f));
    }

    IEnumerator  ClearTile(Vector3Int pos, float time){
        yield return new WaitForSeconds(time);
        m_Tilemap.SetTile(pos, null);
    }
}
