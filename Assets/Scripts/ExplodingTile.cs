using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ExplodingTile : RecieveColideEvent
{
    [System.Serializable]
    public class Blinker
    {
        public float blinkInterval = 0.08f;
        public float numBlinks = 5;
        
        public IEnumerator BlinkTile(Tilemap tilemap, Vector3Int gridPos, TileBase cell)
        {
            for (int i = 0; i < numBlinks; i++)
            {
                tilemap.SetTileFlags(gridPos, TileFlags.None);
                tilemap.SetColor(gridPos, new Color(1.0f, 1.0f, 1.0f, 0.0f));
                yield return new WaitForSeconds(blinkInterval);
                tilemap.SetColor(gridPos, new Color(1.0f, 1.0f, 1.0f, 1.0f));
                yield return new WaitForSeconds(blinkInterval);
            }
        }
    }

    public Blinker blinkingParameters;
    
    public override void EventAt(Vector3Int gridPos, TileBase cell, GameObject who)
    {
        if (who.TryGetComponent(out CharacterLogic cc2d))
        {
            cc2d.touchedExplosiveTile();
            if(m_Tilemap != null)
                StartCoroutine(blinkingParameters.BlinkTile(m_Tilemap, gridPos, cell));
        }
    }
    
}
