﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour
{
    // Start is called before the first frame update
    public float timeToDestroySelf = 0.1f;

    public void Start()
    {
        StartCoroutine(destroySelf());
    }

    // Update is called once per frame
    IEnumerator destroySelf()
    {
        yield return new WaitForSeconds(timeToDestroySelf);
        Destroy(gameObject);
    }
}
