using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleGizmos : MonoBehaviour
{
    public float radius = 0.1f;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, radius);
    }
}
